/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = {
  apps: [
    {
      name: 'email-server',
      exec_mode: 'fork_mode',
      script: './target/release/email-server',
      watch: ['target/release/email-server'],
      ignore_watch: ['src'],
      max_memory_restart: '100M',

      restart_delay: 5000,
      listen_timeout: 3000
    }
  ]
};
