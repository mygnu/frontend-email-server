use config::{ConfigError, Config, File};

#[derive(Debug, Deserialize)]
pub struct Settings {
    pub sparkpost_api_key: String,
    pub receive_email: String,
    pub server_port: String,
    pub sender_email: String,
}

impl Settings {
    pub fn new() -> Result<Self, ConfigError> {
        let mut s = Config::new();
        s.set_default("server_port", "1430")?;
        s.merge(File::with_name("/usr/local/etc/email-server.ini"))?;
//        s.merge(File::with_name("assets/email-server-default.ini").required(false))?;
        s.try_into()
    }
}
